package it.epicode.gestioneEventi.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import it.epicode.gestioneEventi.entities.Concerto;
import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.GaraDiAtletica;
import it.epicode.gestioneEventi.entities.Genere;
import it.epicode.gestioneEventi.entities.Location;
import it.epicode.gestioneEventi.entities.Partecipazione;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;
import it.epicode.gestioneEventi.entities.Persona;
import it.epicode.gestioneEventi.entities.Stato;
import it.epicode.gestioneEventi.persistence.IRepository;
import it.epicode.gestioneEventi.persistence.Repository;
import it.epicode.gestioneEventi.utils.JpaUtil;

public class EventoService implements AbstractEventoService{
	private EntityManager em;
	private IRepository<Evento, Long> eventoRepo;
	private IRepository<Persona, Long> personaRepo;
	private IRepository<Location, Long> locationRepo;
	private IRepository<Partecipazione, Long> partecipazioneRepo;
	
	public EventoService(EntityManager em,IRepository<Evento, Long> repo, IRepository<Persona, Long> personaRepo,
			IRepository<Location, Long> lR, IRepository<Partecipazione, Long> pR) {
		this.personaRepo=personaRepo;
		this.eventoRepo = repo;
		this.em = em;
		this.locationRepo = lR;
		this.partecipazioneRepo = pR;
	}
	

	@Override
	public Evento registraEvento(Evento newEvento) {
		try{
			em.getTransaction().begin();
			eventoRepo.salva(newEvento);
			em.getTransaction().commit();
			return newEvento;
			
		}catch(PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
		
		
	
	}

	@Override
	public Evento getById(Long id) {
	
			Evento ev = eventoRepo.getById(id);
			return ev;	
	}

	@Override
	public void delete(Long id) {
		
		try {
			em.getTransaction().begin();
			eventoRepo.delete(id);
			em.getTransaction().commit();		
		}catch(PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
		
	}


	@Override
	public Partecipazione registraPartecipazione(Long idPersona, Long idEvento) {
		try {
			em.getTransaction().begin();
			Persona pers = personaRepo.getById(idPersona);
			Evento evento = eventoRepo.getById(idEvento);
			Partecipazione part = new Partecipazione();
			part.setEvento(evento);
			part.setPersona(pers);
			part.setStato(Stato.DA_CONFERMARE);
			evento.getPartecipazioni().add(part);
			partecipazioneRepo.salva(part);
			em.getTransaction().commit();
			return part;
		}catch(PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
	}


	@Override
	public Persona registraPersona(Persona p) {
		try {
			em.getTransaction().begin();
			personaRepo.salva(p);
			em.getTransaction().commit();
			return p;
		}catch(PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
	}


	@Override
	public Location registraLocation(Location l) {
		try {
			em.getTransaction().begin();
			locationRepo.salva(l);
			em.getTransaction().commit();
			return l;
		}catch(PersistenceException ex) {
			em.getTransaction().rollback();
			throw ex;
		}
	}


	@Override
	public List<Concerto> getConcertiInStreaming(boolean tF) {
			Query query = em.createQuery("select c from Concerto c where c.inStreaming = : inStreaming");
				query.setParameter("inStreaming",tF);
			List<Concerto> resultList = query.getResultList();
			for(Concerto c: resultList) {
				System.out.println(c.getTitolo() + " " + c.getDataEvento() + " " + c.isInStreaming());
			}
			return resultList;
	}


	@Override
	public List<Concerto> getConcertiPerGenere(List<Genere> listaGeneri) {
		//in permette di scorrere in una lista
			Query query = em.createQuery("select c from Concerto c where c.genere in :generiConcerto");
			query.setParameter("generiConcerto", listaGeneri);
			List<Concerto> resultList = query.getResultList();
			return resultList;
	}


	@Override
	public List<PartitaDiCalcio> getPartiteVinteInCasa() {
		Query query = em.createQuery("select p from PartitaDiCalcio p where p.golSquadraDiCasa > p.golSquadraOspite ");
		List<PartitaDiCalcio> resultList= query.getResultList();
		return resultList;
	}


	@Override
	public List<PartitaDiCalcio> getPartiteVinteInTrasferta() {
		Query query = em.createQuery("select p from PartitaDiCalcio p where p.golSquadraDiCasa < p.golSquadraOspite");
		List<PartitaDiCalcio> resultList = query.getResultList();
		return resultList;
	}


	@Override
	public List<PartitaDiCalcio> getPartitePareggiate() {
		Query query = em.createQuery("select p from PartitaDiCalcio p where p.golSquadraDiCasa == p.golSquadraOspite");
		List<PartitaDiCalcio> resultList = query.getResultList();
		return resultList;
	}


	@Override
	public List<GaraDiAtletica> getGareDiAtleticaPerVincitore(Persona vincitore) {
		Query query = em.createQuery("select g from GaraDiAtletica g where g.vincitore = : vincitore");
		query.setParameter("vincitore", vincitore);
		List<GaraDiAtletica> resultList = query.getResultList();
		return resultList;
	}


	@Override
	public List<GaraDiAtletica> getGareDiAtleticaPerPartecipante(Persona partecipante) {
		Query query = em.createQuery("select g from GaraDiAtletica g where :partecipante member of g.listaAtleti");
		query.setParameter("partecipante", partecipante);
		List<GaraDiAtletica> resultList = query.getResultList();
		return resultList;
	}


	@Override
	public List<Evento> getEventiSoldOut() {
		
		Query query = em.createQuery("select e from Evento e where size(e.partecipazioni) = e.numeroMaxPartecipanti");
		List<Evento> resultList = query.getResultList();
		return resultList;
	}


	@Override
	public List<Evento> getEventiPerInvitato(Persona invitato) {
		Query query = em.createQuery("select e from Evento e where :invitato = (select p from Persona p where p.id in(select part from Partecipazione part, Evento e where part in e.partecipazioni)) ");
		query.setParameter("invitato", invitato);
		List<Evento> resultList = query.getResultList();
		return resultList;
	}


	@Override
	public List<Partecipazione> getPartecipazioniDaConfermarePerEvento(Evento ev) {
		Query query = em.createQuery("select part from Partecipazione part where part.stato = :stato and part.evento.id = :ev");
		query.setParameter("stato", Stato.DA_CONFERMARE);
		query.setParameter("ev", ev.getId());
		List<Partecipazione> resultList = query.getResultList();
		return resultList; 
	}
	
	

}
