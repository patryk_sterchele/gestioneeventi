package it.epicode.gestioneEventi.service;

import java.util.List;

import it.epicode.gestioneEventi.entities.Concerto;
import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.GaraDiAtletica;
import it.epicode.gestioneEventi.entities.Genere;
import it.epicode.gestioneEventi.entities.Location;
import it.epicode.gestioneEventi.entities.Partecipazione;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;
import it.epicode.gestioneEventi.entities.Persona;

public interface AbstractEventoService{
	
    Evento registraEvento(Evento newEvento);
    Evento getById(Long id);
	void delete(Long id);
	Partecipazione registraPartecipazione(Long idPersona, Long idEvento);
	Persona registraPersona(Persona p);
	Location registraLocation(Location l);
	List<Concerto> getConcertiInStreaming(boolean tF);
	List<Concerto> getConcertiPerGenere(List<Genere> listaGeneri);
	List<PartitaDiCalcio> getPartiteVinteInCasa();
	List<PartitaDiCalcio> getPartiteVinteInTrasferta();
	List<PartitaDiCalcio> getPartitePareggiate();
	List<GaraDiAtletica> getGareDiAtleticaPerVincitore(Persona vincitore);
	List<GaraDiAtletica> getGareDiAtleticaPerPartecipante(Persona partecipante);
	List<Evento> getEventiSoldOut();
	List<Evento> getEventiPerInvitato(Persona invitato);
	List<Partecipazione> getPartecipazioniDaConfermarePerEvento(Evento ev);
}
