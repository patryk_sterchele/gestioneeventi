package it.epicode.gestioneEventi.start;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import it.epicode.gestioneEventi.entities.Concerto;
import it.epicode.gestioneEventi.entities.Evento;
import it.epicode.gestioneEventi.entities.GaraDiAtletica;
import it.epicode.gestioneEventi.entities.Genere;
import it.epicode.gestioneEventi.entities.Location;
import it.epicode.gestioneEventi.entities.Partecipazione;
import it.epicode.gestioneEventi.entities.PartitaDiCalcio;
import it.epicode.gestioneEventi.entities.Persona;
import it.epicode.gestioneEventi.entities.Sesso;
import it.epicode.gestioneEventi.entities.Stato;
import it.epicode.gestioneEventi.entities.TipoEvento;
import it.epicode.gestioneEventi.persistence.IRepository;
import it.epicode.gestioneEventi.persistence.Repository;
import it.epicode.gestioneEventi.service.AbstractEventoService;
import it.epicode.gestioneEventi.service.EventoService;
import it.epicode.gestioneEventi.utils.JpaUtil;

public class Main {

	public static void main(String[] args) {
		EntityManager em = null;
		try {
			em = JpaUtil.getEntityManagerFactory().createEntityManager();
			IRepository<Evento, Long> eventoRepo = new Repository<Evento>(em, Evento.class);
			IRepository<Persona, Long> personaRepo = new Repository<Persona>(em, Persona.class);
			IRepository<Location, Long> locationRepo = new Repository<Location>(em, Location.class);
			IRepository<Partecipazione, Long> partecipazioneRepo = new Repository<Partecipazione>(em, Partecipazione.class);
			
			AbstractEventoService ev = new EventoService(em, eventoRepo, personaRepo, locationRepo, partecipazioneRepo);
			
			Set<Partecipazione> setPart1 = new HashSet<>();
			Set<Persona> listaAtleti1 = new HashSet<>();
			
			
			Location l1 = new Location(0, "Duomo", "Milano");
			Location l2 = new Location(0, "Arena", "Verona");
			Location l3 = new Location(0, "Colosseo", "Roma");
			Location l4 = new Location(0, "San Siro", "Milano");
//			ev.registraLocation(l4);
//			ev.registraLocation(l1);
//			ev.registraLocation(l2);
//			ev.registraLocation(l3);
			
			Persona p1 = new Persona(0, "Mario", "Rossi", "mario.rossi@gmail.com", LocalDate.of(1989, 6, 5), Sesso.MASCHIO, new ArrayList<Partecipazione>());
			Persona p2 = new Persona(0, "Luca", "Verdi", "luca.verdi@gmail.com", LocalDate.of(1990, 7, 20), Sesso.MASCHIO, new ArrayList<Partecipazione>());
			Persona p3 = new Persona(0, "Anna", "Bianchi", "anna.bianchi@gmail.com", LocalDate.of(1975, 12, 25), Sesso.FEMMINA, new ArrayList<Partecipazione>());
//			ev.registraPersona(p1);
//			ev.registraPersona(p2);
//			ev.registraPersona(p3);
			
			Evento ev1 = new Concerto(0, "Nabucco", LocalDate.of(2021, 7, 16), "Opera di Giuseppe Verdi", 
					TipoEvento.PUBBLICO, 250, l1, new HashSet<Partecipazione>(), Genere.CLASSICO, false);
			Evento ev2 = new Concerto(0, "OneRepublic", LocalDate.of(2022, 5, 3), "Concerto del gruppo OneRepublic",
					TipoEvento.PUBBLICO, 3500, l3, new HashSet<Partecipazione>(), Genere.POP, true);
			Evento ev3 = new PartitaDiCalcio(0, "Juventus-Milan", LocalDate.of(2021, 8, 24), "Partita di calcio tra Juventus e Milan", TipoEvento.PUBBLICO,
					5000, l4, new HashSet<Partecipazione>(), "Milan", "Juventus", 2, 3);
			Evento ev4 = new PartitaDiCalcio(0, "Juventus-Inter", LocalDate.of(2021, 9, 27), "Partita di calcio tra Juventus e Inter", TipoEvento.PUBBLICO,
					5000, l4, new HashSet<Partecipazione>(), "Inter", "Juventus", 4, 3);
			Evento ev5 = new Concerto(0, "Concerto Privato", LocalDate.of(2021, 12, 29), "Concerto privato per un invitato",
					TipoEvento.PRIVATO, 1, l2, setPart1, Genere.ROCK, false);
			Evento ev6 = new GaraDiAtletica(0, "Atletica 1", LocalDate.of(2022, 4, 15), "Prima gara di atletica",
					TipoEvento.PUBBLICO, 200, l3, new HashSet<Partecipazione>(), listaAtleti1, p2);
//			ev.registraEvento(ev1);
//			ev.registraEvento(ev2);
//			ev.registraEvento(ev3);
//			ev.registraEvento(ev4);
//			ev.registraEvento(ev5);
//			ev.registraEvento(ev6);
			
			listaAtleti1.add(p2);		
		
//			Partecipazione part1 = ev.registraPartecipazione(1L, 5L);
//			setPart1.add(part1);
			
//			List<Genere> listaGeneri = new ArrayList<>();
//			listaGeneri.add(Genere.CLASSICO);
//			listaGeneri.add(Genere.POP);
//			ev.getConcertiInStreaming(true);
//			System.out.println();
//			ev.getConcertiPerGenere(listaGeneri);
			
//			ev.getPartiteVinteInCasa();
//			ev.getPartiteVinteInTrasferta();
//			ev.getEventiSoldOut();
//			ev.getEventiPerInvitato(p1);
//			ev.getPartecipazioniDaConfermarePerEvento(ev5);
//			ev.getGareDiAtleticaPerVincitore(p2);
			
		}finally {
			if(em != null) {
				try {
					em.close();
				}catch(PersistenceException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
