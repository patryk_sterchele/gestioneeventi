package it.epicode.gestioneEventi.entities;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
@Entity
@Table(name="partita_di_calcio")
public class PartitaDiCalcio extends Evento{
	@Column(name="squadra_di_casa")
	private String squadraDiCasa;
	
	@Column(name="squadra_ospite")
	private String squadraOspite;
	
	@Column(name="squadra_vincente")
	private String squadraVincente;
	
	@Column(name="gol_squadra_di_casa")
	private int golSquadraDiCasa;
	
	@Column(name="gol_squadra_ospite")
	private int golSquadraOspite;
	
	public PartitaDiCalcio() {}

	public PartitaDiCalcio(long id, String titolo, LocalDate dataEvento, String descrizione, TipoEvento tipoEvento,
			int numeroMaxPartecipanti, Location location, Set<Partecipazione> sP,String squadraDiCasa, String squadraOspite, int golSquadraDiCasa,
			int golSquadraOspite) {
		super(id, titolo, dataEvento, descrizione, tipoEvento, numeroMaxPartecipanti, location, sP);
		this.squadraDiCasa = squadraDiCasa;
		this.squadraOspite = squadraOspite;
		this.golSquadraDiCasa = golSquadraDiCasa;
		this.golSquadraOspite = golSquadraOspite;
		if(getGolSquadraDiCasa() > getGolSquadraOspite()){
			this.squadraVincente = squadraDiCasa;
		}else if(getGolSquadraDiCasa() < getGolSquadraOspite()) {
			this.squadraVincente = squadraOspite;
		}else {
			this.squadraVincente = null;
		}
	}

	public String getSquadraDiCasa() {
		return squadraDiCasa;
	}

	public void setSquadraDiCasa(String squadraDiCasa) {
		this.squadraDiCasa = squadraDiCasa;
	}

	public String getSquadraOspite() {
		return squadraOspite;
	}

	public void setSquadraOspite(String squadraOspite) {
		this.squadraOspite = squadraOspite;
	}

	public String getSquadraVincente() {
		return squadraVincente;
	}

	public void setSquadraVincente(String squadraVincente) {
			this.squadraVincente = squadraVincente;
	}

	public int getGolSquadraDiCasa() {
		return golSquadraDiCasa;
	}

	public void setGolSquadraDiCasa(int golSquadraDiCasa) {
		this.golSquadraDiCasa = golSquadraDiCasa;
	}

	public int getGolSquadraOspite() {
		return golSquadraOspite;
	}

	public void setGolSquadraOspite(int golSquadraOspite) {
		this.golSquadraOspite = golSquadraOspite;
	}
	
	
	
}
