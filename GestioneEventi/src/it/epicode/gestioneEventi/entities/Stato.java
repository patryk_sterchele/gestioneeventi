package it.epicode.gestioneEventi.entities;

public enum Stato {
	CONFERMATA, DA_CONFERMARE
}
