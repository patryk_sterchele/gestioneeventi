package it.epicode.gestioneEventi.entities;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
@Entity
@Table(name="concerto")
public class Concerto extends Evento{
	@Enumerated(EnumType.STRING)
	private Genere genere;
	
	@Column(name="in_streaming")
	
	private boolean inStreaming;
	
	public Concerto() {}

	public Concerto(long id, String titolo, LocalDate dataEvento, String descrizione, TipoEvento tipoEvento,
			int numeroMaxPartecipanti, Location location, Set<Partecipazione> sP, Genere genere, boolean inStreaming) {
		super(id, titolo, dataEvento, descrizione, tipoEvento, numeroMaxPartecipanti, location, sP);
		this.genere = genere;
		this.inStreaming = inStreaming;
	}

	public Genere getGenere() {
		return genere;
	}

	public void setGenere(Genere genere) {
		this.genere = genere;
	}

	public boolean isInStreaming() {
		return inStreaming;
	}

	public void setInStreaming(boolean inStreaming) {
		this.inStreaming = inStreaming;
	}
	
	
	
}
