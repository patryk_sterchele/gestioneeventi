package it.epicode.gestioneEventi.entities;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="gara_di_atletica")
public class GaraDiAtletica extends Evento{
	
	@ManyToMany(cascade=CascadeType.ALL)
	private Set<Persona> listaAtleti;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_vincitore")
	private Persona vincitore;
	
	public GaraDiAtletica() {}

	public GaraDiAtletica(long id, String titolo, LocalDate dataEvento, String descrizione, TipoEvento tipoEvento,
			int numeroMaxPartecipanti, Location location, Set<Partecipazione> sP, Set<Persona> listaAtleti, Persona vincitore) {
		super(id, titolo, dataEvento, descrizione, tipoEvento, numeroMaxPartecipanti, location, sP);
		this.listaAtleti = listaAtleti;
		this.vincitore = vincitore;
	}

	public Set<Persona> getListaAtleti() {
		return listaAtleti;
	}

	public void setListaAtleti(Set<Persona> listaAtleti) {
		this.listaAtleti = listaAtleti;
	}

	public Persona getVincitore() {
		return vincitore;
	}

	public void setVincitore(Persona vincitore) {
		this.vincitore = vincitore;
	}
	
	
	
}
