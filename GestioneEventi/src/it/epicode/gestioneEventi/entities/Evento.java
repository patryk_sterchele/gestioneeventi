package it.epicode.gestioneEventi.entities;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table ( name = "evento")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Evento {

	@Id
	@SequenceGenerator(name="evento_seq", sequenceName="evento_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="evento_seq")
	private long id;
	private String titolo;
	@Column(name ="data_evento")
	private LocalDate dataEvento;
	private String descrizione;
	@Column(name ="tipo_evento")
	@Enumerated(EnumType.STRING)
	private TipoEvento tipoEvento;
	@Column(name ="numero_max_partecipanti")
	private int numeroMaxPartecipanti;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_location")
	private Location location;
	@OneToMany(mappedBy="evento", cascade=CascadeType.ALL)
	private Set<Partecipazione> partecipazioni;
	
	
	public Evento() {}
	
	public Evento(long id, String titolo, LocalDate dataEvento, String descrizione, TipoEvento tipoEvento,
			int numeroMaxPartecipanti, Location location, Set<Partecipazione> sP) {
		this.id = id;
		this.titolo = titolo;
		this.dataEvento = dataEvento;
		this.descrizione = descrizione;
		this.tipoEvento = tipoEvento;
		this.numeroMaxPartecipanti = numeroMaxPartecipanti;
		this.location = location;
		this.partecipazioni = sP;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getTitolo() {
		return titolo;
	}


	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}


	public LocalDate getDataEvento() {
		return dataEvento;
	}


	public void setDataEvento(LocalDate dataEvento) {
		this.dataEvento = dataEvento;
	}


	public String getDescrizione() {
		return descrizione;
	}


	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}


	public TipoEvento getTipoEvento() {
		return tipoEvento;
	}


	public void setTipoEvento(TipoEvento tipoEvento) {
		this.tipoEvento = tipoEvento;
	}


	public int getNumeroMaxPartecipanti() {
		return numeroMaxPartecipanti;
	}


	public void setNumeroMaxPartecipanti(int numeroMaxPartecipanti) {
		this.numeroMaxPartecipanti = numeroMaxPartecipanti;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Set<Partecipazione> getPartecipazioni() {
		return partecipazioni;
	}

	public void setPartecipazioni(Set<Partecipazione> setPartecipazioni) {
		this.partecipazioni = setPartecipazioni;
	}
	
	
	
	
	
}
