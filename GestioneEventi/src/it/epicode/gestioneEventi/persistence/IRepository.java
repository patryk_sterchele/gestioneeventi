package it.epicode.gestioneEventi.persistence;

public interface IRepository<T,K> {
	

	T salva(T newElement);
	T getById(K id);
	void delete(K id);
	void refresh(T element);

}
